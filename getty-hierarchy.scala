package hierarchy

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import scala.util.Success;

import spray.json._
import DefaultJsonProtocol._
import scala.io.Source

import tree._
import validate._
import cachedhttp._
import sparql._

case class Node(
  id: String,
  labels: Seq[Label]
) {
  def addLabels(labels: Seq[Label]): Node =
    copy(labels = this.labels ++ labels)
  override def toString: String =
    val label = (labels.filter(_.lang == "nl") ++ labels).head.value.replace("<", "").replace(">", "")
    label.head.toUpper +: label.tail
}

@main
def main =
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  import scala.concurrent.ExecutionContext
  implicit val system = ActorSystem[Any](Behaviors.empty, "site")
  implicit val ec: ExecutionContext = system.classicSystem.dispatcher
 
  val source = Source.fromFile("items.json").mkString
  val subjects = source.parseJson.asInstanceOf[JsArray].elements.map(Item.apply)
    .collect { case Success(s) => s }
    .flatMap(_.subjects)
    .toSet
    .filter(_.startsWith("http://vocab"))
    .map(_.split("/")(4))
  val results = subjects
    .flatMap(subject => {
      val query = s"""
        select *  WHERE {
          OPTIONAL {aat:$subject gvp:displayOrder ?bottomDisplayOrder}.
          aat:$subject xl:prefLabel/xl:literalForm ?bottomLabel.
          aat:$subject gvp:broaderPreferredExtended ?child.
          OPTIONAL {?child gvp:displayOrder ?childDisplayOrder}.
          ?child xl:prefLabel/xl:literalForm ?childLabel.
          ?child gvp:broaderPreferred ?parent.
          OPTIONAL {?parent gvp:displayOrder ?parentDisplayOrder}.
          ?parent xl:prefLabel/xl:literalForm ?parentLabel.
          filter(lang(?bottomLabel) = "nl" || lang(?bottomLabel) = "en").
          filter(lang(?childLabel) = "nl" || lang(?childLabel) = "en").
          filter(lang(?parentLabel) = "nl" || lang(?parentLabel) = "en")
        }
      """
      val encoded = URLEncoder.encode(query, StandardCharsets.UTF_8.toString());
      val result = getCached(s"aat-$subject", s"http://vocab.getty.edu/sparql.json?query=${encoded}&_implicit=false&implicit=true&_equivalent=false&_form=%2Fsparql")
      val elements = result.parseJson.asInstanceOf[JsObject].fields("results").asInstanceOf[JsObject].fields("bindings").asInstanceOf[JsArray].elements
      if (elements.isEmpty) Nil
      else {
        val children = elements.map(_.asInstanceOf[JsObject].fields("child")).map(Uri(_)).map(_.split("/").last)
        val parents = elements.map(_.asInstanceOf[JsObject].fields("parent")).map(Uri(_)).map(_.split("/").last)
        // find the child that isn't also a parent (then it's the parent of the bottom)
        val firstParentId = (children.toSet -- parents).head
        val firstParentLabels = elements.filter(e => {
          Uri(e.asInstanceOf[JsObject].fields("child")).split("/").last == firstParentId
        }).map(_.asInstanceOf[JsObject].fields("childLabel")).map(Label(_))
        val firstParent = Node(firstParentId, firstParentLabels)
        val bottom = Node(subject, elements.map(e => Label(e.asInstanceOf[JsObject].fields("bottomLabel"))).toSet.toSeq)
        (firstParent, bottom) +: elements.map(e => {
          val o = e.asInstanceOf[JsObject]
          val parentId = Uri(o.fields("parent")).split("/").last
          val parent = Node(parentId, Seq(Label(o.fields("parentLabel"))))
          val childId = Uri(o.fields("child")).split("/").last
          val child = Node(childId, Seq(Label(o.fields("childLabel"))))
          (parent, child)
        })
      }
    })
  val nodeBag = (results.map(_._1) ++ results.map(_._2)).foldLeft(Tree[String, Node](_.id))((acc, node) => {
    acc.addOrUpdate(node, _.addLabels(node.labels))
  })
  val tree = results.foldLeft(nodeBag){ case (acc, (parent, child)) => acc.connect(parent.id, child.id) }
  def printTree(id: String, depth: Int = 0): Unit =
    tree.get(id) match {
      case None => println(s"$id not found?")
      case Some(node) =>
        println(s"<li><a href='#${node.id}'>$node</a></li>")
        val children = tree.children(id)
        if (children.nonEmpty)
          println("<ul>")
          children.foreach(c => printTree(c, depth + 2))
          println("</ul>")
    }
  println("<ul>")
  tree.roots.foreach(root => printTree(root))
  println("</ul>")
