package tree

import scala.collection.immutable.MultiDict

// Tree where you can add vertices even before the
// node data is known
private case class TreeData[K, V](
  nodes: Map[K, V],
  parents: Map[K, K],
  children: MultiDict[K, K],
)
class Tree[K, V](getId: V=>K, data: TreeData[K, V]) {
  def get(key: K): Option[V] = data.nodes.get(key)
  def children(key: K): Seq[K] = data.children.get(key).toSeq
  def add(element: V): Tree[K, V] =
    new Tree(getId, TreeData(
      data.nodes ++ Seq((getId(element), element)),
      data.parents,
      data.children
    ))
  def addOrUpdate(element: V, update: V => V): Tree[K, V] =
    add(data.nodes.get(getId(element)).map(update).getOrElse(element))
  def connect(parent: K, child: K): Tree[K, V] =
    new Tree(getId, TreeData(
      data.nodes,
      data.parents ++ Seq(child -> parent),
      data.children + (parent -> child)
    ))
  def roots: Set[K] =
    data.children.keySet.toSet -- data.children.values.toSet

  override def toString = data.nodes.mkString(", ")
}
object Tree {
  def apply[K, V](getId: V=>K): Tree[K, V] =
    new Tree(
      getId,
      TreeData(
        Map.empty,
        Map.empty,
        MultiDict.empty))
}
