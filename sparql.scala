package sparql

import spray.json._

case class Label(value: String, lang: String)
object Label {
  def apply(js: JsValue): Label =
    Label(
      js.asInstanceOf[JsObject].fields("value").asInstanceOf[JsString].value,
      js.asInstanceOf[JsObject].fields("xml:lang").asInstanceOf[JsString].value
    )
}
object Uri {
  def apply(js: JsValue): String =
    js.asInstanceOf[JsObject].fields("value").asInstanceOf[JsString].value
}
