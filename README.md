Some random scripts for mangling Omeka S data.

For now without any pagination, since our collection is still small and we can just fetch everything. Some
of the scripts require you to fetch things manually:

    curl https://collectie.eicas.nl/api/items?per_page=200 > items.json

or

    curl https://bibliotheek.eicas.nl/api/items?per_page=1000 > items.json

then

    sbt run
