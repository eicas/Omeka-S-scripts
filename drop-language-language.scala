package droplanguagelanguage

import scala.concurrent.ExecutionContext
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source

import spray.json._
import DefaultJsonProtocol._

case class Item(
  id: Long,
  languages: Seq[(String, Option[String])],
  obj: JsObject
  )

object Item {
    def apply(json: JsValue): Item = {
        val obj = json.asInstanceOf[JsObject]
        Item(
          obj.fields("o:id").asInstanceOf[JsNumber].value.toLong,
          obj.fields.get("dcterms:language").toList.flatMap(
          _.asInstanceOf[JsArray].elements.map(e => {
            val o = e.asInstanceOf[JsObject]
            (
              o.fields("@value").asInstanceOf[JsString].value,
              o.fields.get("@language").map(_.asInstanceOf[JsString].value)
            )
          })),
          obj)
    }
}

@main
def main =
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  given system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "foo")
  val source = Source.fromFile("items.json").mkString
  source
    .parseJson
    .asInstanceOf[JsArray]
    .elements
    .map(Item.apply)
    .filter(_.languages.exists(_._2.isDefined))
    .map { item =>
      import org.apache.pekko.http.scaladsl.client.RequestBuilding.Patch
      import org.apache.pekko.http.scaladsl.model._
      import org.apache.pekko.http.scaladsl.model.HttpEntity
      import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
      import SprayJsonSupport._
      given ExecutionContext = system.executionContext
      Patch(
        s"https://bibliotheek.eicas.nl/api/items/${item.id}?key_identity=TODO&key_credential=TODO",
        JsObject(
          item.obj.fields.map {
              case ("dcterms:language", _) =>
                "dcterms:language" -> 
                  JsArray(item.obj.fields("dcterms:language").asInstanceOf[JsArray].elements.map {
                      case lang: JsObject =>
                        JsObject(lang.fields.filter(_._1 != "@language"))
                  })
              case other => other
          }
        )
      ).toStrict(10.seconds)
    }
    .map(Await.result(_, 10.seconds))
    .foreach { req =>
      import org.apache.pekko.http.scaladsl.model._
      import org.apache.pekko.http.scaladsl._
      println(req)
      println(req.entity.asInstanceOf[HttpEntity.Strict].data.utf8String)
      //given ExecutionContext = system.executionContext
      //val res = Await.result(Http().singleRequest(req), 10.seconds)
      //println(res)
    }
