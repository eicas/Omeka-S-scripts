package validate

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import org.apache.pekko.actor.typed._
import org.apache.pekko.actor.typed.scaladsl._

import spray.json._
import DefaultJsonProtocol._

import cachedhttp.getCached
import dotenv.env

// TODO:
// dcterms:description beginnen met een hoofdletter
// foaf:weblog: begint met https
// ZERO-validatie: altijd hoofdletters
// Nul-beweging

// browsen op thesaurus

// Overzicht onze aanvullingen
// op thesaurus

// Overzicht 'Relation' - dit zijn allemaal kunstenaars,
// maar deels niet in RKD te vinden.

case class Item(
  id: Long,
  owner: Option[String],
  resourceTemplate: Long,
  resourceClass: String,
  sites: Seq[JsValue],
  title: Option[String],
  creators: Seq[String],
  description: Option[String],
  issued: Seq[String],
  issue: Option[String],
  languages: Seq[(String, Option[String])],
  locators: Seq[String],
  subjects: Seq[String],
  obj: JsObject)

def resourceClassById(obj: JsObject, id: Long): String = try {
    id match {
      case 18 => "Policy"
      case 25 => "Event"
      case 26 => "Image"
      case 36 => "Article"
      case 38 => "AudioVisualDocument"
      case 40 => "Book"
      case 41 => "BookSection"
      case 44 => "Code"
      case 45 => "CollectedDocument"
      case 47 => "Conference"
      case 49 => "Document"
      case 59 => "Interview"
      case 64 => "LegalDocument"
      case 67 => "Magazine"
      case 82 => "Report"
      case 88 => "Thesis"
      // TODO choose between Person and Project for donations
      case 94 => "Person"
      case 99 => "Project"
    }
  } catch {
      case _: MatchError =>
        throw new IllegalStateException(s"New resource class: https://bibliotheek.eicas.nl/api/resource_classes/${id} in ${obj}")

  }
def ownerById(id: Long): String = id match {
    case 1 => "Arnout"
    case 2 => "Ingrid"
    case 3 => "Marloes"
    case 4 => "Sija"
    case 5 => "Petra"
    case 6 => "Liz"
}

object Item {
    given (JsValue => String) = _.asInstanceOf[JsString].value
    implicit def headOption[T](in: Seq[JsValue])(using extractor: JsValue => T): Option[T] = in.headOption.map(extractor)
    implicit def all[T](in: Seq[JsValue])(using extractor: JsValue => T): Seq[T] = in.map(extractor)
    private def extract[T](o: JsObject, field: String)(using extractor: Seq[JsValue] => T): T =
      extractor(o.fields.get(field).toSeq
        .flatMap(_.asInstanceOf[JsArray].elements
        .map{ v => 
          val i = v.asInstanceOf[JsObject]
          i.fields.get("@value").getOrElse(i.fields("@id"))
        }))
    def extractIdOpt(obj: JsObject, field: String): Option[Long] =
        obj.fields(field) match 
          case JsNull => None
          case _ => Some(extractId(obj, field))
    def extractId(obj: JsObject, field: String): Long =
        try {
            obj.fields(field).asInstanceOf[JsObject].fields("o:id").asInstanceOf[JsNumber].value.toLong
        } catch {
            e => throw new Exception(s"Failed to extract id field '$field' in: $obj", e);
        }

    def apply(json: JsValue): Try[Item] = {
        val obj = json.asInstanceOf[JsObject]
        val id = obj.fields("o:id").asInstanceOf[JsNumber].value.toLong
        try {
            Success(Item(
          id,
          extractIdOpt(obj, "o:owner").map(ownerById),
          extractId(obj, "o:resource_template"),
          resourceClassById(obj, extractId(obj, "o:resource_class")),
          obj.fields("o:site").asInstanceOf[JsArray].elements,
          extract(obj, "dcterms:title"),
          extract(obj, "dcterms:creator"),
          extract(obj, "dcterms:description"),
          obj.fields.get("dcterms:issued").toList.flatMap(_.asInstanceOf[JsArray]
            .elements
            .map(obj => {
                obj.asInstanceOf[JsObject]
                  .fields("@value")
                  .asInstanceOf[JsString]
                  .value
            })),
          extract(obj, "bibo:issue"),
          obj.fields.get("dcterms:language").toList.flatMap(
            _.asInstanceOf[JsArray].elements.map(e => {
              val o = e.asInstanceOf[JsObject]
              (
                o.fields("@value").asInstanceOf[JsString].value,
                o.fields.get("@language").map(_.asInstanceOf[JsString].value)
              )
            })),
          obj.fields.get("bibo:locator").toList.flatMap(
            _.asInstanceOf[JsArray].elements.map(e => {
              e.asInstanceOf[JsObject]
                .fields("@value")
                .asInstanceOf[JsString]
                .value
            })
          ),
          extract(obj, "dcterms:subject"),
          obj))
        } catch {
            case e => Failure(new Exception(s"Failed to parse $id", e))
        }
    }
}

case class Validation(name: String, validate: Item => Option[String])

def editLink(item: Item, suffix: Option[String] = None): String =
  s"- https://bibliotheek.eicas.nl/admin/item/${item.id}/edit${suffix.getOrElse("")} (${item.owner})"

def editLink(item: Item, suffix: String): String =
  editLink(item, Some(suffix))

def getObjects()(using system: ActorSystem[_]): Seq[JsObject] =
  val source = getCached("bieb-all", s"https://bibliotheek.eicas.nl/api/items?per_page=1000&key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}")
  source
    .parseJson
    .asInstanceOf[JsArray]
    .elements
    .map(_.asInstanceOf[JsObject])

def getItems()(using system: ActorSystem[_]): Seq[Item] =
  getObjects().map(Item.apply).collect { case Success(s) => s }

@main
def main =
  implicit val system = ActorSystem[Any](Behaviors.empty, "site")
  val items = getItems()
  val failures =
    getObjects().map(Item.apply).collect { case Failure(e) => e }
  //items
  //  .filter(_.title.contains("Kunstforum International"))
  //  .groupBy(_.issued.head)
  //  .toList
  //  .sortBy(_._1)
  //  .foreach { case (year, issues) =>
  //    println(s"$year: ${issues.flatMap(_.issue).mkString(", ")}")
  //  }

  val validations = Seq(
    Validation(
      "Items without a site",
      i => Option.when(i.sites.isEmpty)(editLink(i, "#sites"))
    ),
    Validation(
      "Items with invalid dates",
      i => {
        val problems = i.issued.filter(value => {
          // For winter/summer we might want to switch to 
          // https://www.loc.gov/standards/datetime/
          // but then we'd have to update visualization
          // as well
          // https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http%3a%2f%2fpurl.org%2fdc%2fterms%2fdate
          // https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http%3a%2f%2fpurl.org%2fdc%2felements%2f1.1%2fdate
          value.isEmpty || (value != "z.j." && value.replace("Winter ", "").replace("Zomer ", "").replaceAll("\\?$", "").exists(d => !"0123456789/".contains(d)))
        })
        Option.when(problems.nonEmpty)("https://bibliotheek.eicas.nl/admin/item/" + i.id + "/edit - " + problems.mkString("'", ", ", "'"))
      }
    ),
    Validation(
      "Items with non-ISO-639-1 language codes",
      i => {
        val problems =
          i
            .languages
            .map(_._1)
            .flatMap(value =>
              if (Seq("nl", "en", "de", "fr", "it", "es", "zh", "id").contains(value))
                  None
              else
                  Some(value)
            )
        Option.when(problems.nonEmpty)(editLink(i, " - " + problems.mkString("'", ", ", "'")))
      }
    ),
    Validation(
      "Items with languages for the language",
      i => {
        val problems =
          i
            .languages
            .flatMap(_._2)
        Option.when(problems.nonEmpty)(editLink(i, " - " + problems.mkString("'", ", ", "'")))
      }
    ),
    Validation(
      "Descriptions should start with uppercase characters",
      i => {
        i.resourceClass match {
          case "Event" | "Project" => None
          case _ =>
            val problems = i.description.filter(_.head.isLower)
            Option.when(problems.nonEmpty)(editLink(i, " - " + problems.map(_.split(' ').head).mkString("'", ", ", "'")))
        }
      }
    ),
    Validation(
      "Creators should all have their own field",
      i => {
        Option.when(i.creators.exists(_.trim().contains("\n")))(editLink(i))
      }
    ),
    Validation(
      "Values should not have additional whitespace",
      i => {
        val fieldsWithWhitespaceValue = 
          i.obj.fields.flatMap {
            case (key, JsArray(values)) =>
              Option.when(values.collect { case o: JsObject => o }.exists(_.fields.get("@value").exists(i => {
                val v = i.asInstanceOf[JsString].value
                v != v.trim
              })))(key)
            case _ => None
          }
        Option.when(fieldsWithWhitespaceValue.nonEmpty)(editLink(i, " - " + fieldsWithWhitespaceValue.mkString(", ")))
      }
    ),
    Validation(
      "Locators should have the expected form",
      i => {
        Option.when(!i.locators.forall(l =>
          l == "KC" ||
          """[A-Z]{2}/[A-Z]{3}[A-Z]?""".r.matches(l))
        )(editLink(i) + " " + i.locators)
      }
    )
  )

  println(s"Parse failures:")
  failures.foreach(_.printStackTrace())
  validations.foreach: v =>
    println(s"${v.name}:")
    items
      .flatMap(v.validate)
      .foreach(println)
