package register

import spray.json._
import DefaultJsonProtocol._
import scala.io.Source

case class Item(identifier: String, title: String, created: String):
  override def toString = s"E$identifier	$title	$created"

  def identifierNumeric: Int = Integer.parseInt(identifier.tail)

def get(element: JsValue, key: String): String =
  element match {
    case JsObject(fields) =>
      fields(key) match {
        case JsArray(elements) =>
          if (elements.length == 1)
            elements(0).asInstanceOf[JsObject].fields("@value").asInstanceOf[JsString].value
          else
            throw new IllegalStateException(s"Found ${elements.length} values for $key in $element")
        case JsObject(fields) =>
            fields("@value").asInstanceOf[JsString].value
      }
  }

@main
def register = 
  val source = Source.fromFile("items.json").mkString
  val items = source
    .parseJson
    .asInstanceOf[JsArray]
    .elements
    .map(element =>
      val obj = element.asInstanceOf[JsObject]
      Item(
        get(obj, "dcterms:identifier"),
        get(obj, "dcterms:title"),
        get(obj, "o:created"))
    )
    .sortBy(_.identifierNumeric)
  println(items.mkString("\n"))
