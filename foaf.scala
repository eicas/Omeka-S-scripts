package foaf

import spray.json._
import DefaultJsonProtocol._

def literal(id: Integer, label: String, value: String): JsValue = JsArray(JsObject(
    "type" -> JsString("literal"),
    "property_id" -> JsNumber(id),
    "property_label" -> JsString(label),
    "is_public" -> JsTrue,
    "@value" -> JsString(value)
  ))
def uri(propertyId: Integer, propertyLabel: String, id: String, label: String): JsValue = JsArray(JsObject(
    "type" -> JsString("uri"),
    "property_id" -> JsNumber(propertyId),
    "property_label" -> JsString(propertyLabel),
    "is_public" -> JsTrue,
    "@id" -> JsString(id),
    "o:label" -> JsString(label),
  ))

def weblog(id: String, label: String): (String, JsValue) = ("foaf:weblog", uri(148, "weblog", id, label))
def theme(label: String): (String, JsValue) = ("foaf:theme", literal(175, "theme", label))
