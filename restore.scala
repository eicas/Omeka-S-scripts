package restore

import scala.concurrent._
import scala.concurrent.duration._

import spray.json._
import DefaultJsonProtocol._

import org.apache.pekko.http.scaladsl._
import org.apache.pekko.http.scaladsl.client.RequestBuilding.Post
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import SprayJsonSupport._

import cachedhttp.getCached
import dotenv.env

@main
def main =
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  given system: ActorSystem[Any] = ActorSystem[Any](Behaviors.empty, "site")
  val (id, item) = getCached("bieb-all", s"https://bibliotheek.eicas.nl/api/items?per_page=1000&key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}")
    .parseJson
    .asInstanceOf[JsArray]
    .elements
    .map(json => (json.asInstanceOf[JsObject].fields("o:id").asInstanceOf[JsNumber].value, json))
    .filter(_._1 == 1678)
    .head
  println(s"XXX: [$item]")
  val url = s"https://bibliotheek.eicas.nl/api/items"
  given ExecutionContext = system.executionContext
  val post = Post(
    s"$url?key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}",
    item
  )
  //val result = Await.result(Http().singleRequest(post), 20.seconds)
  //println(result)
  
