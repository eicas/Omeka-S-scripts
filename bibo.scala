package bibo

import spray.json._
import DefaultJsonProtocol._

def literal(id: Integer, label: String, value: String): JsValue = JsArray(JsObject(
    "type" -> JsString("literal"),
    "property_id" -> JsNumber(id),
    "property_label" -> JsString(label),
    "is_public" -> JsTrue,
    "@value" -> JsString(value)
  ))

def content(value: String): (String, JsValue) = ("bibo:content", literal(91, "content", value))
def isbn(value: String): (String, JsValue) =
  if value.length == 10 then
    ("bibo:isbn10", literal(100, "isbn10", value))
  else if value.length == 13 then
    ("bibo:isbn13", literal(101, "isbn13", value))
  else ???
def issn(value: String): (String, JsValue) = ("bibo:issn", literal(102, "issn", value))
def issue(value: String): (String, JsValue) = ("bibo:issue", literal(103, "issue", value))
def numPages(value: String): (String, JsValue) =
  if value.forall(_.isDigit) then
    ("bibo:numPages", literal(106, "number of pages", value + " p."))
  else
    ("bibo:numPages", literal(106, "number of pages", value))
def locator(file: String, title: String, author: Option[String]): (String, JsValue) =
  val prefix =
    if (file == "CVS bestand Dada.csv") "KC/" else "HK/"
  val postfix = author.map { a =>
    (
      if (a.endsWith("red.]")) then
        title.take(4)
      else
        a.take(4)
    ).toUpperCase
  }.getOrElse("")
  ("bibo:locator", literal(105, "locator", prefix+postfix))
