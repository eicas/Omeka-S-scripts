package seegers

import scala.io.Source
import spray.json._
import DefaultJsonProtocol._

import dotenv._

case class Classification(className: String, classId: Int, templateId: Int)

def typeClass(t: String, title: String): Classification =
  if title == "Kunstforum International" then
    Classification("bibo:Magazine", 67, 10)
  else if t == "CD" then
    Classification("bibo:AudioDocument", 38, 13)
  else
    Classification("bibo:Book", 40, 1)

object Main extends App {
  val files = Seq(
    "CSV Arte Povera.csv",
    "CSV Beuys.csv",
    "CSV Zero.csv",
    "CSV bestand Documenta.csv",
    "CSV Kunstforum.csv",
    "CVS bestand Dada.csv",
  )
  var hasdata: Set[Int] = Set.empty
  val objects = files.flatMap { file =>
    println(file)
    Source.fromFile("schenking-seegers/" + file)
      .mkString
      .replaceAll("\r\r\n", "\r")
      .split("\n")
      .drop(1)
      .map { line =>
        val columns = line.tail.init.split("\",\"")
        assert(columns.size == 66)
        Range(0, 66).foreach { i =>
          if (columns(i) != "")
              hasdata += i
        }
        val (title, theme) = { 
          val v = columns(57)
          if (v.startsWith("Kunstforum")) {
            ("Kunstforum International", 
             Some(v.drop("Kunstforum International no".size).dropWhile(_ match {
              case '.' => true
              case ' ' => true
              case ',' => true
              case '/' => true
              case '-' => true
              case n => n.isDigit
            })))
          } else {
            (v, None)
          }
        }
        assert(title != "")

        // TODO a number of columns are filled only rarely, let's process those manually:
        // 2 (notes)
        // 5 (accompanying_material)
        // 18 (corporate_author)
        // 27 (illustrations)
        // 47 (series_title)

        val author = Option.when(columns(4) != "")(columns(4)).map { a =>
          columns(3) match {
            case "" => a
            case "Editors" | "Edited by" | "Editor" | "coordinanting editor" | "Redactie" => a + " [red.]"
            case o => 
              a
          }
        }

        val website =
          if (title == "Kunstforum International") then
            Some(Option.when(columns(20) != "")((columns(20), columns(20))).getOrElse(("https://www.kunstforum.de", "Kunstforum International")))
          else
            Option.when(columns(20) != "")((columns(20), columns(20)))

        val format = Option.when(columns(24) != "")(columns(24).replaceAll("\\.", ""))
        val isbn = Option.when(columns(33) != "")(columns(33).replaceAll("-", ""))
        isbn.foreach(i => assert(i.size == 10 || i.size == 13))
        val issn = Option.when(title == "Kunstforum International")(".")
        val issued = Option.when(columns(35) != "")(columns(35)).map {
            case "1963? (niet gedateerd)" => "1963?"
            case "n.d." => "s.a."
            case o => o
        }.getOrElse("s.a.")
        val `abstract` = Option.when(columns(54) != "")(columns(54))
        val comments = Option.when(columns(39) != "")(columns(39))
        val content = Option.when(`abstract`.isDefined || comments.isDefined)(List(`abstract`, comments).flatten.mkString("\n")).getOrElse(".")
        val pages = Option.when(columns(40) != "")(columns(40)).getOrElse("ongepagineerd")
        val issue = Option.when(columns(46) != "")(columns(46)).map(
          _.dropWhile { _ match
              case 'n' | 'o' | 'r' | ' ' | '.' | ':' => true
              case _ => false
          }.takeWhile(_.isDigit))
        
        val relation = 
          if (title == "Kunstforum International") then
            Some(Option.when(columns(41) != "")(columns(41)).getOrElse("."))
          else
            Option.when(columns(41) != "")(columns(41))
        // in overleg laten vervallen
        // val placeOfPublication = Option.when(columns(42) != "")(columns(42))
        // TODO 43(priref) useful to keep?
        val language = Option.when(columns(56) != "")(columns(56)).map {
            case "Duits" => "de"
            case "Engels" => "en"
            case "Nederlands" => "nl"
            case "Italiaans" => "it"
        }.getOrElse("de")
        val keyword = Option.when(columns(58) != "")(columns(58))
        val publisher = Option.when(columns(59) != "")(columns(59)).getOrElse("Documenta GmbH")
        val classification = typeClass(columns(38), title)
        JsObject(Map(
          "@context" -> JsString("https://bibliotheek.eicas.nl/api-context"),
          "@type" -> JsArray(
            JsString("o:Item"),
            JsString(classification.className)
          ),
          "o:is_public" -> JsTrue,
          "o:owner" -> ref("users", 1),
          "o:resource_class" -> ref("resource_classes", classification.classId),
          "o:resource_template" -> ref("resource_templates", classification.templateId),
          "o:title" -> JsString(title),
          "o:site" -> ref("sites", 3),
          dcterms.title(title),
          dcterms.creator(author.getOrElse("n.n.")),
          bibo.locator(file, title, author),
          dcterms.issued(issued),
          dcterms.isPartOf(1417, "Seegers"),
          bibo.numPages(pages),
          dcterms.publisher(publisher),
          dcterms.language(language),
          bibo.content(content),
          dcterms.subject(keyword),
        ) ++ Seq(
          theme.map(foaf.theme),
          isbn.map(bibo.isbn),
          issn.map(bibo.issn),
          issue.map(bibo.issue),
          website.map { case (uri, label) => foaf.weblog(uri, label) },
          relation.map(dcterms.relation),
        ).flatten
        )
      }
  }

  import scala.concurrent.Await
  import scala.concurrent.ExecutionContext
  import scala.concurrent.duration._
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  import org.apache.pekko.http.scaladsl._
  import org.apache.pekko.http.scaladsl.client.RequestBuilding.Post
  import org.apache.pekko.http.scaladsl.model._
  import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
  import SprayJsonSupport._

  implicit val system: ActorSystem[Any] = ActorSystem(Behaviors.empty, "site")
  given ExecutionContext = system.executionContext
  print(objects.head)

  for (obj <- objects) yield {
    //val result = Await.result(Http().singleRequest(
    //  Post(
    //    s"https://bibliotheek.eicas.nl/api/items?key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}",
    //    obj
    //  )
    //), 20.seconds)
    //result.status match {
    //  case StatusCodes.UnprocessableEntity =>
    //    val entity = Await.result(result.entity.toStrict(20.seconds, 200000), 20.seconds)
    //    println(entity.data.utf8String)
    //  case StatusCodes.OK =>
    //    println("OK")
    //}
  }

  def ref(t: String, id: Int): JsValue =
    JsObject(
      "@id" -> JsString(s"https://bibliotheek.eicas.nl/api/$t/$id"),
      "o:id" -> JsNumber(id)
    )
  val columns = Source.fromFile("schenking-seegers/" + files(0)).mkString.split("\n").head.drop(2).init.split("\",\"")
}
