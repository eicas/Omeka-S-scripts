package bieb_museumjournaal_partof

import scala.concurrent.Await
import scala.concurrent.duration._

import cachedhttp._
import dotenv._

import spray.json._
import DefaultJsonProtocol._

import org.apache.pekko.http.scaladsl._
import org.apache.pekko.http.scaladsl.client.RequestBuilding.Put
import org.apache.pekko.http.scaladsl.model._
import org.apache.pekko.http.scaladsl.model.HttpEntity
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import SprayJsonSupport._

@main
def main =
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  import scala.concurrent.ExecutionContext
  implicit val system = ActorSystem[Any](Behaviors.empty, "site")
  val bieb = getCached("bieb-all", s"https://bibliotheek.eicas.nl/api/items?per_page=1000&key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}")

  given ExecutionContext = system.executionContext
  bieb.parseJson.asInstanceOf[JsArray].elements
    .map(_.asInstanceOf[JsObject])
    .filter(o => Seq("Kunst & Museumjournaal", "Museumjournaal").contains(o.fields("o:title").asInstanceOf[JsString].value))
    .map(o => {
      val url = o.fields("@id").asInstanceOf[JsString].value
      // Would much prefer PATCH here, if we could change the semantics as
      // discussed at https://github.com/omeka/omeka-s/issues/1719#issuecomment-1295763861
      Put(
        s"$url?key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}",
        JsObject(
          o.fields ++ Map(dcterms.isPartOf(1719, "Wil Njio"))
        )
      )
    })
    .drop(1)
    .map(put => {
      println(put)
      //println(s"${put.fields("o:title")} ${put.fields("@id")}")
      Await.result(Http().singleRequest(put), 20.seconds)
    })
