package site

import spray.json._
import DefaultJsonProtocol._
import scala.io.Source

import cachedhttp.getCached
import dotenv.env


def get(obj: JsObject, key: String): Seq[JsValue] =
  obj.fields(key) match {
    case JsArray(elements) =>
      elements.map(_.asInstanceOf[JsObject].fields("@value"))
    case JsObject(fields) =>
      List(fields("@value"))
  }


case class BibliotheekItem(id: Integer, title: Seq[String], site: Option[JsValue]):
  override def toString = s"$id: ${title.mkString(",")}, $site"

object BibliotheekItem:
  def apply(element: JsValue): BibliotheekItem =
    val obj = element.asInstanceOf[JsObject]
    new BibliotheekItem(
        obj.fields("o:id").asInstanceOf[JsNumber].value.intValue,
        get(obj, "dcterms:title").map(_.asInstanceOf[JsString].value),
        obj.fields("o:site").asInstanceOf[JsArray].elements.headOption
        )

@main
def main =
  import org.apache.pekko.actor.typed._
  import org.apache.pekko.actor.typed.scaladsl._
  import scala.concurrent.ExecutionContext
  given system: ActorSystem[Any] = ActorSystem[Any](Behaviors.empty, "site")
  implicit val ec: ExecutionContext = system.classicSystem.dispatcher
  val source = getCached("bieb-all", s"https://bibliotheek.eicas.nl/api/items?per_page=1000&key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}")
  val items = source
    .parseJson
    .asInstanceOf[JsArray]
    .elements
    .map(BibliotheekItem(_))
    .filter(i => i.site.isEmpty)
    .map(item =>
      import org.apache.pekko.http.scaladsl.client.RequestBuilding.Patch
      import org.apache.pekko.http.scaladsl.model._
      // Warning: Patch works for `o:` resources
      // but not for rdf properties,
      // https://github.com/omeka/omeka-s/issues/1719#issuecomment-1295763861
      // TODO get credentials from .env like
      // in bieb-rm-medium.scala
      Patch(
        s"https://bibliotheek.eicas.nl/api/items/${item.id}?key_identity=${env("BIEB_ID")}&key_credential=${env("BIEB_CRED")}",
        HttpEntity(
          ContentTypes.`application/json`,
          s"""
          {
            "o:site": [
              {
                "@id": "https://bibliotheek.eicas.nl/api/sites/3",
                "o:id": 3
              }
            ]
          }
          """)
      )
    )
    .map { req =>
      import org.apache.pekko.http._
      import org.apache.pekko.http.scaladsl._
      import scala.concurrent.duration._
      import scala.concurrent._
      Await.result(Http(system.classicSystem).singleRequest(req), 10.seconds)
      req
    }
  println(items.size)

