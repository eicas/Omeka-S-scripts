package dcterms

import spray.json._
import DefaultJsonProtocol._

def resource(propertyId: Integer, propertyLabel: String, valueResourceId: Int, title: String): JsValue = JsArray(JsObject(
    "type" -> JsString("resource"),
    "property_id" -> JsNumber(propertyId),
    "property_label" -> JsString(propertyLabel),
    "is_public" -> JsTrue,
    "@id" -> JsString(s"https://bibliotheek.eicas.nl/api/items/$valueResourceId"),
    "value_resource_id" -> JsNumber(valueResourceId),
    "value_resource_name" -> JsString("items"),
    "display_title" -> JsString(title),
))
def literal(id: Integer, label: String, value: String): JsValue = JsArray(JsObject(
    "type" -> JsString("literal"),
    "property_id" -> JsNumber(id),
    "property_label" -> JsString(label),
    "is_public" -> JsTrue,
    "@value" -> JsString(value)
  ))
def rkd(propertyId: Integer, propertyLabel: String, value: (Int, String)): JsValue = JsArray(JsObject(
    (Seq("type" -> JsString("valuesuggest:ndeterms:rkdartists"),
    "property_id" -> JsNumber(propertyId),
    "property_label" -> JsString(propertyLabel),
    "is_public" -> JsTrue
    ) ++ (if (value._1 == 0) Seq("@value" -> JsString(value._2)) else Seq(
    "@id" -> JsString(s"https://data.rkd.nl/artists/${value._1}"),
    "o:label" -> JsString(value._2)))).toMap
  ))
def aat(propertyId: Integer, propertyLabel: String, id: Int, label: String): JsValue = JsArray(JsObject(
  (Seq(
    "type" -> JsString("valuesuggest:ndeterms:aat"),
    "property_id" -> JsNumber(propertyId),
    "property_label" -> JsString(propertyLabel),
    "is_public" -> JsTrue,
    ) ++ (if (id == 0) Seq("@value" -> JsString(label)) else Seq(
    "@id" -> JsString(s"http://vocab.getty.edu/aat/$id"),
    "o:label" -> JsString(label),
    ))).toMap
  ))



def title(value: String): (String, JsValue) = ("dcterms:title", literal(1, "Title", value))
def language(value: String): (String, JsValue) = ("dcterms:language", literal(12, "Language", value))
def publisher(value: String): (String, JsValue) = ("dcterms:publisher", literal(5, "Publisher", value))
def creator(value: String): (String, JsValue) = ("dcterms:creator", literal(2, "Creator", value))
def format(value: String): (String, JsValue) = ("dcterms:format", literal(9, "Format", value))
def issued(value: String): (String, JsValue) = ("dcterms:issued", literal(23, "Date Issued", value))
def relation(name: String): (String, JsValue) = ("dcterms:relation", rkd(13, "Relation", name match {
  case "Hilgemann, Ewerdt" => (38412, "Hilgemann, Ewerdt")
  case "Anselmo, Giovanni (1934)" => (202631, "Anselmo, Giovanni")
  case "Zorio, Gilberto (1944)" => (207359, "Zorio, Gilberto")
  case "Kounellis, Jannis (1936)" => (46116, "Kounellis, Jannis")
  case "Beuys, Joseph (1921-1986)" => (7928, "Beuys, Joseph")
  case "Byars, James Lee (1932-1997)" => (14591, "Byars, James Lee")
  case "Castellani, Enrico (1930-    )" => (15840, "Castellani, Enrico")
  case "Peeters, Henk (1925-2013)" => (62366, "Peeters, Henk")
  case "Girken, Raumund (1930-2002)" => (236355, "Girken, Raimund")
  case "Enwezor, Okwui" => (345421, "Enwezor, Okwui")
  case "David, Catherine" => (344743, "David, Catherine")
  case "Hoet, Jan" => (337521, "Hoet, Jan")
  case "Haftmann, Werner" => (346901, "Haftmann, Werner")
  case "Breton, André (1896-1966)" => (12420, "Breton, André")
  case "Tzara, Tristan (1896-1963)" => (105573, "Tzara, Tristan")
  case "." => (0, "x")
}))
def subject(name: Option[String]): (String, JsValue) = 
  val (id, label) = name.flatMap(_ match {
    case "Arte povera" => Some(300047851, "Arte Povera")
    case "Documenta" => None
    case "Tentoonstellingscatalogus" => Some(300026096, "tentoonstellingscatalogi")
    case "Interviews" => Some(300026392, "interviews")
    case "Interview" => Some(300026392, "interviews")
    case "Multiples" => Some(300178624, "multipels")
    case "Oeuvrecatalogus" => Some(300026061, "oeuvre catalogi")
    case "Installatie" => Some(300047896, "installaties (kunstwerken)")
    case "Performance kunst" => Some(300121445, "performances (live)")
    case "Zero" => Some(0, "ZERO")
    case "Nul-beweging" => Some(0, "Nul-beweging")
    case "Dada" => Some(300021500, "dada")
    case "Constructivisme" => Some(300021393, "constructivistisch")
  }).getOrElse((0, "."))
  ("dcterms:subject", aat(3, "Subject", id, label))
def isPartOf(itemId: Int, label: String): (String, JsValue) = ("dcterms:isPartOf", resource(33, "Is Part Of", itemId, label))
