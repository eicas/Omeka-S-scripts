package dotenv

import scala.io.Source

val env: Map[String, String] =
  Source
    .fromFile(".env")
    .mkString
    .split("\n")
    .map(n => (n.split("=")(0), n.split("=")(1)))
    .toMap
