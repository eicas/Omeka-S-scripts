scalaVersion := "3.3.1"

val PekkoVersion = "1.0.2"
val PekkoHttpVersion = "1.0.0"

//resolvers += "pekko-http-snapshot-repository" at "https://repository.apache.org/content/repositories/snapshots"
resolvers += "apache-staging" at "https://repository.apache.org/content/groups/staging/"

libraryDependencies ++= Seq(
  ("io.spray" %%  "spray-json" % "1.3.6"),
  ("org.apache.pekko" %% "pekko-actor-typed" % PekkoVersion),
  ("org.apache.pekko" %% "pekko-stream" % PekkoVersion),
  ("org.apache.pekko" %% "pekko-http" % PekkoHttpVersion),
  ("org.apache.pekko" %% "pekko-http-spray-json" % PekkoHttpVersion),
  "commons-codec" % "commons-codec" % "1.15",
  "org.scala-lang.modules" %% "scala-collection-contrib" % "0.3.0",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
)
